import argparse


parser = argparse.ArgumentParser(prog='cifar10-cnn', description='cnn on cifar10 dataset')
parser.add_argument('--batch-size', dest='batch_size', metavar='M', type=int, default=16, help='batch size for training and validation')
parser.add_argument('--epochs', dest='epochs', metavar='E', type=int, default=1, help='number of epochs')
parser.add_argument('--enable-debug', dest='debug', metavar='D', default=False, help='produce debug logs for tensorboard')
args = parser.parse_args()

config = {'batch_size' : args.batch_size, 
          'epochs' : args.epochs,
          'debug': args.debug}


from trainer import train

train(config)
