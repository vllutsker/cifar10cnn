from tensorflow.keras.layers import Conv2D, MaxPool2D, Input, Flatten, Activation, Dropout
from tensorflow.keras.layers import Layer
from tensorflow.keras.models import Model
from tensorflow.keras.losses import CategoricalCrossentropy

    
class PooledConv2D(Layer):
    def __init__(self, nkernels, kernel_size):
        super(PooledConv2D, self).__init__()
        self.conv = Conv2D(nkernels, kernel_size=kernel_size, activation='relu', padding='valid')
        self.pool = MaxPool2D((2,2))
        
    def call(self, inputs):
        x = self.conv(inputs)
        return self.pool(x)

def Classifier():
    """Classifier template for 32x32 pixel images with 10 categories"""
    def result(inp):
        x = PooledConv2D(50, 5)(inp) # 14x14
        x = Dropout(0.2)(x)
        x = PooledConv2D(50, 3)(x) # 6x6
        x = Conv2D(120, kernel_size=3, activation='relu', padding='valid')(x) # 4x4
        x = Dropout(0.2)(x)
        x = PooledConv2D(240, 3)(x) # 2x2        
        x = Dropout(0.2)(x)
        x = Conv2D(10, kernel_size=1, padding='valid')(x)
        x = Flatten()(x)
        out = Activation('softmax')(x)
        return out
    return result

def SimpleClassifierModel():
    def result():
        inp = Input((32,32,3))
        out = Classifier()(inp)

        model = Model(inputs=inp, outputs=out, name='classifier')
        model.summary()

        model.compile(optimizer='adam', loss=CategoricalCrossentropy(), metrics=['accuracy'])
        return model
    return result
